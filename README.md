mgbackup
========

A tool to make backups spliting the data in multiples MEGA accounts.

The application use the library [https://github.com/t3rm1n4l/go-mega](https://github.com/t3rm1n4l/go-mega) to access to MEGA.

### Features
  - Create backups from folders
  - Restore remote dump

### Usage
    Usage ./mgbackup:
        Backup a folder: mgbackup -b <folder_local_path>
        Restore a remote dump: mgbackup -r <folder_local_path> <folder_local_destination>

### TODO
  - Use an unlimited number of accounts
  - List remote dumps
